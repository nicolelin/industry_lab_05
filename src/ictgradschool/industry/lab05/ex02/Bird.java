package ictgradschool.industry.lab05.ex02;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */

public class Bird implements IAnimal {

    @Override
    public String sayHello() {
        return "tweet tweet";
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public String myName() {
        return "Tweety";
    }

    @Override
    public int legCount() {
        return 2;
    }
}

/*
Tweety the bird says tweet tweet.
Tweety the bird is a non-mammal.
Did I forget to tell you that I have 2 legs.
 */