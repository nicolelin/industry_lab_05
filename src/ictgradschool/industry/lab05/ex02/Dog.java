package ictgradschool.industry.lab05.ex02;

import ictgradschool.industry.lab05.ex04.farmmanager.animals.Animal;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */

public class Dog implements IAnimal {

    @Override
    public String sayHello() {
        return "woof woof";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        return "Bruno";
    }

    @Override
    public int legCount() {
        return 4;
    }
}

/*
Bruno the dog says woof woof.
Bruno the dog is a mammal.
Did I forget to tell you that I have 4 legs.
 */