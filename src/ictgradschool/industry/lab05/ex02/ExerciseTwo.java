package ictgradschool.industry.lab05.ex02;

import ictgradschool.industry.lab05.ex04.farmmanager.animals.Animal;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.
        for (int i = 0; i < list.length; i++) {

            System.out.println(list[i].myName() + " the " + list[i].getClass().getSimpleName() + " says " + list[i].sayHello() + '.');
            System.out.print(list[i].myName() + " the " + list[i].getClass().getSimpleName() + " is a " /* (list[i].isMammal() ? "mammal" : "non-mammal") + '.' */);

            if (list[i].isMammal()) {
                System.out.print("mammal");
            } else if (!list[i].isMammal()) {
                System.out.print("non-mammal");
            }

            System.out.print('.');

            System.out.println('\n' + "Did I forget to tell you that I have " + list[i].legCount() + " legs.");

            // TODO If the animal also implements IFamous, print out that corresponding info too.

            if (list[i] instanceof IFamous) {

                // Cast list[i] into IFamous -> changing scope from IAnimal to IFamous

                IFamous animal = (IFamous) list[i];

                System.out.println("This is a famous name of my animal type: " + animal.famous());
            }

            System.out.println("--------------------------------------------------------------"); // LINE BREAK

        }

// if (listfamous[i].famous() != null && !listfamous[i].famous().isEmpty()) { // IGNORE THIS - was a method for checking if String is not empty.

    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
