package ictgradschool.industry.lab05.ex04.farmmanager;

import ictgradschool.industry.lab05.ex04.farmmanager.animals.Animal;
import ictgradschool.industry.lab05.ex04.farmmanager.animals.Cow;
import ictgradschool.industry.lab05.ex04.farmmanager.animals.Horse;

import java.util.List;

import java.util.ArrayList;

public class Farm {
	private ArrayList<Animal> animals;
	private final int startingMoney;
	private int money;

	public Farm(int money) {
		this.money = money;
		this.startingMoney = money;

		// An ArrayList is like an array, except it can hold any number of elements.
		// We will properly learn how to use lists and other collections in a later lecture.
		this.animals = new ArrayList<>();
	}

	public Farm() {
		this(10000);
	}

	// Time counter
	public void tick() {
		for (int i = 0; i < animals.size(); i++) {
			animals.get(i).tick();
		}
	}

	// Harvest
	public void harvest() {
		for (int i = 0; i < animals.size(); i++) {
			Animal a = animals.get(i);
			if (a instanceof ProductionAnimals) {
				ProductionAnimals produceA = (ProductionAnimals) a;
				if (produceA.harvestable()) {
					System.out.println("Your " + a.getName() + " is ready to harvest!");
//					System.out.println("DEBUG before " + money);
					money += produceA.harvest();
//					System.out.println("DEBUG after " + money);
				}
			}
		}
	}


	/**
	 * Returns how much money the farm currently has.
	 */
	public int getMoney() {
        return this.money;
	}

	/**
	 * Returns how much money the farm started with.
	 */
	public int getStartingMoney() {
        return this.startingMoney;
	}

	/**
	 * Purchases an animal and bills the farm for it.
	 *
	 * @param animalName The name of the type of animal you wish to buy.
	 * @return true if the animal can be bought, false otherwise.
	 */
	public boolean buyAnimal(String animalName) {
		Animal newAnimal;
		switch (animalName) {
			case "cow":
				newAnimal = new Cow();
				break;

			case "horse":
				newAnimal = new Horse();
				break;

			default:
				// Animal not recognized.
				return false;
		}

		int price = (int) (newAnimal.getValue() * 1.15);

		System.out.println("DEBUG before " + money);

		if (money >= price) {
			money -= price;

			System.out.println("DEBUG after " + money);


		} else {

			// Insufficient funds.
			return false;
		}

		animals.add(newAnimal);
		return true;
	}

	/**
	 * Sells all of the stock on the farm.
	 */
	public int sell() {

		// Calculate the value of all of your animals
		int totalPrice = 0;

		// The size() method of an ArrayList is the same as the length field of an Array.
		for (int i = 0; i < animals.size(); i++) {

			// Calling the get method on an ArrayList is the same as using the square braces on an Array.
			// For example, if animals were an Array, the next line would look like this:
			// Animal a = animals[i];
			Animal a = animals.get(i);
			totalPrice += a.getValue();
		}

		// Remove the animals from your farm and add their value to your money.
		// The clear() method of an ArrayList removes all items from it.
		animals.clear();
		money += totalPrice;
		return totalPrice;
	}

	/**
	 * Go through each animal and if you have enough money to feed it,
	 * subtract the cost to feed from your money and call the feed method on the animal.
	 */
	public void feedAll() {
		//TODO
		// Calculate how much it takes to feed all the animals in stock
		// Add value to animal fed
		// Total money > stock * cost to feed individually
		// Animals contain an array of all stock
		// Accumulate all stock cost into a total cost

		int totalCost = 0;

		for (int i = 0; i < animals.size(); i++) {
			totalCost = totalCost + animals.get(i).costToFeed();
		}

		// Check if we have enough money to feed all

		if (totalCost < money) {
			money = money - totalCost;
			}

		for (int i = 0; i < animals.size(); i++) {
			animals.get(i).feed();

		}

	}

    /**
     * Go through each animal and if it is of the type specified and you have enough money to feed it,
     * subtract the cost to feed from your money and call the feed method on the animal.
     */
    public void feed(String animalType) {
        //TODO

//		animals.getClass()
//		animalType - what the player inputted
//		boolean to test animals.getClass().equals(animalType)

		for (int i = 0; i < animals.size(); i++) {
			int costToFeed = animals.get(i).costToFeed();
			if (animals.get(i).getClass().getSimpleName().toLowerCase().equals(animalType.toLowerCase()) && money >= costToFeed) {
				money -= animals.get(i).costToFeed();
				animals.get(i).feed();
			}
//			else if (!animals.get(i).getClass().getSimpleName().toLowerCase().equals(animalType.toLowerCase())) {
//				System.out.println("You have entered an invalid animal to feed. Please try again.");
//			}
			else if (money < costToFeed) {
				System.out.println("You don't have enough money to feed " + animals.get(i).getClass().getSimpleName());
			}
		}
    }

	/**
	 * Prints out a list of all of the stock on the farm.
     * If there are no animals in stock inform the user.
	 */
	public void printStock() {
		//TODO
//		ArrayList animals = new ArrayList();

		if (animals.size() > 0) {
			System.out.println("You have " + animals);
		} else {
			System.out.println("You have no animals in stock.");

		}
	}

// Old Code. Attempt to print list of all animals - loop not working
//		for (int i = 0; i < animals.size(); i++) {
//
//			Animal a = animals.get(i);
//
//			if (animals.size() > 0) {
//				System.out.println("You have " + animals.size() + ' ' + animals.get(i).getName() + " !!! DEBUG !!!" + animals);
//			} else {
//				System.out.println("You have no animals in stock.");
//
//			}
//		}

	}
