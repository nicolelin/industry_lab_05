package ictgradschool.industry.lab05.ex04.farmmanager.animals;

import ictgradschool.industry.lab05.ex04.farmmanager.ProductionAnimals;

public class Horse extends Animal implements ProductionAnimals {

	/** All horse instances will have the same, shared, name: "Horse" */
	private static final String name = "Horse";

	public Horse() {
		super.name = name;
		value = 1200;
	}

	public void feed() {
		if (value < 2000) {
			value += 100;
		}
	}

	public int costToFeed() {
		return 60;
	}

//	public String getName() {
//		return name;
//	}
	
//	public int getValue() {
//		return value;
//	}

	public String toString() {
		return name + " - $" + value;
	}

	@Override
	public boolean harvestable() { // Check if tick is 10
		if (super.time >= 10) {
			return true;
		}
		return false;
	}

	@Override
	public int harvest() { // Increase money by 30, Reduce ticks by 10
		if (harvestable()) {
			time = time - 10;
			return 30;
		}
		return 0;
	}

}
