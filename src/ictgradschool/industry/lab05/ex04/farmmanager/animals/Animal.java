package ictgradschool.industry.lab05.ex04.farmmanager.animals;

abstract public class Animal {
	protected int time;
	protected int value;
	protected String name;

	public abstract void feed();

	public abstract int costToFeed();

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	public void tick()	{
		time +=1;
		System.out.println("Time counter: " + name + " age: " + time);
	}

}
