package ictgradschool.industry.lab05.ex04.farmmanager.animals;

import ictgradschool.industry.lab05.ex04.farmmanager.ProductionAnimals;

public class Cow extends Animal implements ProductionAnimals {

	/** All cow instances will have the same, shared, name: "Cow" */
	private static final String name = "Cow";

	public Cow() {
		super.name = name;
		value = 1000;
	}

	public void feed() {
		if (value < 1500) {
			value += 100;
		}
	}

	public int costToFeed() {
		return 60;
	}

//	public String getName() {
//		return name;
//	}
	
//	public int getValue() {
//		return value;
//	}

	public String toString() {
		return name + " - $" + value;
	}

	@Override
	public boolean harvestable() { // Check if tick is 10
		if (super.time >= 10) {
			return true;
		}
		return false;
	}

	@Override
	public int harvest() { // Increase money by 20, Reduce ticks by 10
		if (harvestable()) {
			time = time - 10;
			return 20;
		}
		return 0;
	}

}