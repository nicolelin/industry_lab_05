package ictgradschool.industry.lab05.ex03.Ex03Q1;

/**
 * Created by ylin183 on 20/03/2017.
 */

public class Ex03Q1 {
    public void start() {
        SuperClass s1 = new SuperClass();
        Test1 t1 = new Test1();
    }

    public static void main(String[] args) {
        SuperClass s1 = new SuperClass(); // Execute SuperClass function when calling s1
        Test1 t1 = new Test1(); // Execute Test1 function when calling t1

        System.out.println("The Base object");
        System.out.println("S1.x = " + s1.x);
        /*
        Before constructor:
        x = 10
        y = 10

        Run constructor SuperClass:
        x = y
        x = 10

        y = 10 + 1
        y = 11
         */

        System.out.println("S1.y = " + s1.y);
        /*
        Before constructor:
        x = y
        x = 11

        Run constructor SuperClass:
        y = 11 + 1
        y = 12
         */

        System.out.println("S1.foo() = " + s1.foo());
        /*
        ! Returns value of x. Does not call SuperClass !

        foo() return x
        x = 10
         */

        System.out.println("S1.goo() = " + s1.goo());
        /*
        ! Returns value of y. Does not call SuperClass !

        foo() return y
        x = 12
         */

        /////////////////////////////////////////////////////

        System.out.println("\nThe Derived object");
        System.out.println("\nInherited fields");

        System.out.println("T1.x = " + t1.x);
        /*
        ! Make a new instance of x for T1.x !

        Before constructor:
        x = 10
        y = 11

        Run constructor:
        x = y
        x = 11

        y = 11 + 1
        y = 12

        */

        /*
        DEBUG CONSOLE OUTPUT

args = {String[0]@457}
s1 = {SuperClass@458}
 x = 10
t1 = {Test1@459}
 x2 = 20
 x = 11
t1.x = 11
t1.y = 12
         */

        System.out.println("T1.y = " + t1.y);
        /*

        Grab value of y in t1 (from before):
        x = 11
        y = 12

         */

        /*
        DEBUG CONSOLE OUTPUT

args = {String[0]@457}
s1 = {SuperClass@458}
 x = 10
t1 = {Test1@459}
 x2 = 20
 x = 11
t1.x = 11
t1.y = 12

         */

        System.out.println("T1.foo() = " + t1.foo());

        System.out.println("T1.goo() = " + t1.goo());


        /////////////////////////////////////////////////////

        System.out.println("\nThe instance/class fields");

        System.out.println("T1.x2 = " + t1.x2);
        System.out.println("T1.y2 = " + t1.y2);
        System.out.println("T1.foo2() = " + t1.foo2());
        System.out.println("T1.goo2() = " + t1.goo2());
    }

}

// Output //

/*
The Base object
S1.x = 10
S1.y = 12
S1.foo() = 10
S1.goo() = 12

The Derived object

Inherited fields
T1.x = 11
T1.y = 12
T1.foo() = 11
T1.goo() = 12

The instance/class fields
T1.x2 = 20
T1.y2 = 21
T1.foo2() = 20
T1.goo2() = 21
 */