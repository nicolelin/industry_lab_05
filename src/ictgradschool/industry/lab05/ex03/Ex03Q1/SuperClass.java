package ictgradschool.industry.lab05.ex03.Ex03Q1;

/**
 * Created by ylin183 on 20/03/2017.
 */

public class SuperClass {
    public int x = 10; // Instance variable - s1 and t1 are separate instances of x
    static int y = 10; // Static variable

    SuperClass() {
        x = y++ ;
    }

    public int foo() {
        return x;
    }

    public static int goo() {
        return y;
    }
}