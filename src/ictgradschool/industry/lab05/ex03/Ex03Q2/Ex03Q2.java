package ictgradschool.industry.lab05.ex03.Ex03Q2;

/**
 * Created by ylin183 on 20/03/2017.
 */
public class Ex03Q2 {
    public void start() {
        SuperClass s2 = new Test1();
    }

    public static void main(String[] args) {
        SuperClass s2 = new Test1();
        System.out.println("\nThe static Binding");
        System.out.println("S2.x = " + s2.x);
        //
        /*

        Before constructor:
        x = 10
        y = 10

        Run constructor:

        x = y = 10
        y = 10 + 1
        y = 11

         */

        System.out.println("S2.y = " + s2.y);
        System.out.println("S2.foo() = " + s2.foo());
        System.out.println("S2.goo2() = " + s2.goo());
    }
}

// Output //

/*
The static Binding
S2.x = 10
S2.y = 11
S2.foo() = 10
S2.goo2() = 11
 */