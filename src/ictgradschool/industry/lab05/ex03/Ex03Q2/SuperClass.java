package ictgradschool.industry.lab05.ex03.Ex03Q2;

/**
 * Created by ylin183 on 20/03/2017.
 */

public class SuperClass {
    public int x = 10;
    static int y = 10;

    SuperClass() {
        x = y++ ;
    }

    public int foo() {
        return x;
    }

    public static int goo() {
        return y;
    }
}